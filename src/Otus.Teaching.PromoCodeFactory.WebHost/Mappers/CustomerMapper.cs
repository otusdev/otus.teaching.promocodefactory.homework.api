﻿﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomServiceGrpc;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class CustomerMapper
    {

        public static Customer MapFromModel(CreateOrEditCustomerRequest model, IEnumerable<Preference> preferences, Customer customer = null)
        {
            if(customer == null)
            {
                customer = new Customer();
                customer.Id = Guid.NewGuid();
            }
            
            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();
            
            return customer;
        }
        public static List<PreferenceResponseGrpc> MapListPreferenceResponse(IEnumerable<PreferenceResponse> preferences)
        {
            List<PreferenceResponseGrpc> preferencesList = new List<PreferenceResponseGrpc>();
            foreach (var preference in preferences)
            {
                preferencesList.Add(new PreferenceResponseGrpc()
                {
                    Id = preference.Id.ToString(),
                    Name = preference.Name,
                });
            }
            return preferencesList;
        }

        public static CustomerResponseGrpc MapFromModel(CustomerResponse customer)
        {
            List<PreferenceResponseGrpc> preferences = MapListPreferenceResponse(customer.Preferences.ToList());
            CustomerResponseGrpc response = new CustomerResponseGrpc
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            };
            response.Preferences.AddRange(preferences);
            return response;

        }

    }
}
