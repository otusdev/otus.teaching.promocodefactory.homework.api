﻿using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.SignalR;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerSignalRApi
{

    public interface ICustomerHub
    {
        Task SendAllCustomersResponse(List<CustomerShortResponse> customers);
        Task SendGetCustomerResponse(CustomerResponse customer);

        Task SendCreateResponse(CustomerResponse customer);
        Task SendEditCustomerResponse(string message);
        Task SendDeleteCustomerResponse(string message);
    }

    public class CustomerHub : Hub<ICustomerHub>
    {
        private readonly Otus.Teaching.PromoCodeFactory.WebHost.CustomerService _customerService;


        public CustomerHub(Otus.Teaching.PromoCodeFactory.WebHost.CustomerService customerService)
        {
            _customerService = customerService;
        }

        public async Task GetAllCustomers()
        {
            await Clients.Caller.SendAllCustomersResponse(await _customerService.GetCustomersAsync());
        }

        public async Task GetCustomer(Guid id)
        {
            try
            {
                var result = await _customerService.GetCustomerAsync(id);
                await Clients.Caller.SendGetCustomerResponse(result);
            }
            catch (ArgumentException)
            {
                throw new HubException($"Customer with id {id} is required.");
            }
        }

        public async Task CreateCustomer(CreateOrEditCustomerRequest request)
        {
            var result = await _customerService.CreateCustomerAsync(request);
            await Clients.Caller.SendCreateResponse(result);
        }

        public async Task DeleteCustomer(Guid id)
        {
            try
            {
                await _customerService.DeleteCustomerAsync(id);
                await Clients.Caller.SendDeleteCustomerResponse($"Элемент с ид {id} удален");
            }
            catch (ArgumentException)
            {
                throw new HubException($"Customer with id {id} is required.");
            }
        }

        public async Task EditCustomers(Guid id, CreateOrEditCustomerRequest request)
        {
            try
            {
                await _customerService.EditCustomersAsync(id, request);
                await Clients.Caller.SendEditCustomerResponse($"Элемент с ид {id} изменен");
            }
            catch (ArgumentException)
            {
                throw new HubException($"Customer with id {id} is required.");
            }
        }
    }
}
