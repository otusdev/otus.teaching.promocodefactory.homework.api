﻿using Microsoft.AspNetCore.SignalR.Client;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using SirnalRClient;
using System;
using System.Text.Json;
using System.Threading.Tasks;

namespace SignalRConsoleClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:5001/hubs/customer")
                .Build();

            connection.Closed += async (error) =>
            {
                await Task.Delay(new Random().Next(0, 5) * 1000);
                await connection.StartAsync();
            };

            connection.On("SendAllCustomersResponse", (JsonElement customersElement) =>
            {
                // Обработка ответа от сервера на вызов метода SendAllCustomersResponse
                var customers = JsonSerializer.Deserialize<List<Customer>>(customersElement.GetRawText());
                Console.WriteLine("Received all customers:");
                foreach (var customer in customers)
                {
                    Console.WriteLine($"FirstName: {customer.firstName}, Email: {customer.email}");
                }
            });           

            await connection.StartAsync();

            Console.WriteLine("Choose an action:");
            Console.WriteLine("1. Get all customers");
            Console.WriteLine("0. Exit");
            Console.Write("Enter your choice: ");
            string choice = "";
            while (choice != "0")
            {
                choice = Console.ReadLine();
                Guid customerId;
                switch (choice)
                {
                    case "1":
                        await connection.InvokeAsync("GetAllCustomers");
                        break;
                    case "0":

                        break;
                    default:
                        Console.WriteLine("Invalid choice.");
                        break;
                }
            }
            Console.ReadLine();
        }
    }
}
